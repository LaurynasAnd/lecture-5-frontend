import { takeLatest, put, call } from "redux-saga/effects";
import axios from "axios";
import {
  FETCH_TODOS,
  FETCH_TODOS_FAILURE,
  FETCH_TODOS_SUCCESS,
} from "./todoActionTypes";

export function* fetchTodos() {
  try {
    const { data: todos } = yield call(
      axios.get,
      "http://localhost:3001/todos"
    );
    yield put({ type: FETCH_TODOS_SUCCESS, payload: todos });
  } catch (error) {
    yield put({ type: FETCH_TODOS_FAILURE });
  }
}

export default function* todosSaga() {
  yield takeLatest(FETCH_TODOS, fetchTodos);
}
