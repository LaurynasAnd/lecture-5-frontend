import axios from "axios";
import { FETCH_TODOS_SUCCESS } from "../todoActionTypes";
import { fetchTodos } from "../todosSaga";

describe("fetchTodos", () => {
  it("should fetch todos", () => {
    const saga = fetchTodos();

    const first = saga.next();
    expect(first.done).toBe(false);
    expect(first.value.type).toBe("CALL");
    expect(first.value.payload.fn).toBe(axios.get);
    expect(first.value.payload.args[0]).toBe("http://localhost:3001/todos");

    const todos = [{ id: 123 }];

    const second = saga.next({ data: todos });
    expect(second.done).toBe(false);
    expect(second.value.type).toBe("PUT");
    expect(second.value.payload.action).toEqual({
      type: FETCH_TODOS_SUCCESS,
      payload: todos,
    });

    expect(saga.next().done).toBe(true);
  });
});
