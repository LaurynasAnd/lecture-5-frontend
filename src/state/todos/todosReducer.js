import {
  ADD_TODO,
  DELETE_TODO,
  FETCH_TODOS,
  FETCH_TODOS_FAILURE,
  FETCH_TODOS_SUCCESS,
  TOGGLE_TODO,
} from "./todoActionTypes";

function handleToggleTodo(todos, id) {
  const index = todos.findIndex((todo) => todo.id === id);
  const todo = todos[index];

  const changedTodo = {
    ...todo,
    completed: !todo.completed,
  };

  return [...todos.slice(0, index), changedTodo, ...todos.slice(index + 1)];
}

const initialState = {
  todos: [],
  isLoading: false,
};

export default function todosReducer(state = initialState, action = {}) {
  switch (action.type) {
    case ADD_TODO:
      return {
        ...state,
        todos: [
          ...state.todos,
          {
            ...action.payload,
            completed: false,
          },
        ],
      };
    case TOGGLE_TODO:
      return {
        ...state,
        todos: handleToggleTodo(state.todos, action.payload.id),
      };
    case DELETE_TODO:
      return {
        ...state,
        todos: state.todos.filter((todo) => todo.id !== action.payload.id),
      };
    case FETCH_TODOS:
      return {
        ...state,
        isLoading: true,
      };
    case FETCH_TODOS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        todos: action.payload,
      };
    case FETCH_TODOS_FAILURE:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
}
