import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import Layout from "./components/Layout";
import HomePage from "./containers/HomePage";
import NotesPage from "./containers/NotesPage";
import NotFoundPage from "./containers/NotFoundPage";
import TodoListPage from "./containers/TodoListPage";
import store from "./state/store";
import EmployeesPage from "./containers/EmployeesPage/EmployeesPage";

export default function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Layout>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/todos" component={TodoListPage} />
            <Route path="/notes" component={NotesPage} />
            <Route path="/employees" component={EmployeesPage} />
            <Route path="*" component={NotFoundPage} />
          </Switch>
        </Layout>
      </BrowserRouter>
    </Provider>
  );
}
