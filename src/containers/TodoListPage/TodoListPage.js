import React, { useEffect } from "react";
import PropTypes from "prop-types";
import TodoList from "./components/TodoList";
import AddTodoForm from "./components/AddTodoForm";

export default function TodoListPage({
  todos,
  toggleTodo,
  deleteTodo,
  addTodo,
  fetchTodos,
  isLoading,
}) {
  useEffect(() => {
    fetchTodos();
  }, [fetchTodos]);

  return (
    <>
      {isLoading ? (
        "Loading..."
      ) : (
        <TodoList
          todos={todos}
          toggleTodo={toggleTodo}
          deleteTodo={deleteTodo}
        />
      )}
      <AddTodoForm addTodo={addTodo} />
    </>
  );
}

TodoListPage.propTypes = {
  todos: PropTypes.array.isRequired,
  toggleTodo: PropTypes.func.isRequired,
  deleteTodo: PropTypes.func.isRequired,
  addTodo: PropTypes.func.isRequired,
  fetchTodos: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
};
