import { connect } from "react-redux";
import TodoListPage from "./TodoListPage";
import {
  addTodo,
  toggleTodo,
  deleteTodo,
  fetchTodos,
} from "../../state/todos/todosActions";

const mapStateToProps = (state) => ({
  todos: state.todos.todos,
  isLoading: state.todos.isLoading,
});

const mapDispatchToProps = {
  addTodo,
  toggleTodo,
  deleteTodo,
  fetchTodos,
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoListPage);
