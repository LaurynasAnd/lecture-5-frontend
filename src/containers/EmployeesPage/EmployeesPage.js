import React, { useState, useEffect } from "react";
import axios from "axios";

export default function EmployeesPage() {
  const [quote, setQuote] = useState("");

  useEffect(() => {
    const request = axios.get("https://quotes.rest/qod");
    request.then((response) => {
      setQuote(response.data.contents.quotes[1].quote);
    });
  }, []);

  return <div>{quote}</div>;
}
