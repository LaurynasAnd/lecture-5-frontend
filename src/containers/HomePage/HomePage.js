import React, { useState } from "react";

export default function HomePage() {
  const [quote] = useState("");

  // Fetch quote here

  return (
    <div>
      <h1>Home Page</h1>
      <div>
        <span>Quote of the day: </span>
        <span>{quote}</span>
      </div>
    </div>
  );
}
